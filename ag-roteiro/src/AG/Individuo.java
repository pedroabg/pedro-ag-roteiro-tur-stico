package AG;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import Atrativos.Atrativo;
import Atrativos.Valores;

public class Individuo implements Comparable<Individuo> {

	int fitness;
	int[] genoma;
	// 1 coopera 0 n�o coopera
	Atrativo[] atrativos;
	int geracao;
	Populacao populacao;
	Double fDistancia;
	Double fPreco;
	Double fAvaliacao;
	Double fCompatibilidade;

	static MersenneTwisterFast prng = new MersenneTwisterFast();

	double probMutacaco;

	public int getFitness() {
		return fitness;
	}

	public void setFitness(int fitness) {
		this.fitness = fitness;
	}

	public int[] getGenoma() {
		return genoma;
	}

	public void setGenoma(int[] genoma) {
		this.genoma = genoma;
	}

	public Individuo() {
		super();
		fitness = -1;
		probMutacaco = 0.4;

	}

	static Individuo GerarIndividuo(int numGenes, int geracao) {

		Individuo individuo = new Individuo();
		individuo.genoma = new int[numGenes];
		individuo.setGeracao(geracao);
		
		List<Integer> genesAleatorios = gerarGenesAleatorios(); 

		for (int i = 0; i < numGenes; i++) {

			individuo.genoma[i] = genesAleatorios.get(i);
		}

		
		// individuo.populacao = populacao;

		return individuo;
	}

	private static List<Integer> gerarGenesAleatorios() {
		// TODO Auto-generated method stub
		List<Integer> genesAleatorios = new ArrayList<Integer>();
		
		for (int i = 1; i <= Config.NUM_ALFABETO; i++) {
			genesAleatorios.add(i);
		}
		Collections.shuffle(genesAleatorios);
		return genesAleatorios;
	}






	public int compareTo(Individuo o) {
		// TODO Auto-generated method stub

		return o.getFitness() - this.fitness;
	}

	public int getGeracao() {
		return geracao;
	}

	public void setGeracao(int geracao) {
		this.geracao = geracao;
	}



	public void avaliar() {
		Double finalFitness = new Double(0);
		double D, P, T, A;		
		this.decodificaAtrativo();
		int n = this.getAtrativos().length;
		
		this.fDistancia = Fitness.calcularPercurso(this);
		// invers�o. botar 1/distancia m�dia
		D = (1/(this.fDistancia/n))*Config.Pd;
//		this.fDistancia = D;
		fPreco = Fitness.calcularPreco(this);
		P = (1/(fPreco/n))*Config.Pp;
//		fPreco = P;
		
		fCompatibilidade = Fitness.calcularCompatibilidade(this);
		T = (fCompatibilidade/n)*Config.Pt;
//		fCompatibilidade = T;
	
		fAvaliacao = Fitness.calcularAvaliacao(this);
		A = (fAvaliacao)*Config.Pa;
		
//		fAvaliacao = A;
		
		finalFitness += (D + P + T + A) ;		
		
		finalFitness += Fitness.penalizar(this);
		
		
		this.fitness = finalFitness.intValue();
	
	}

	public int checaFitness(Individuo parceiro) {
		int fitness = 0;
		int gene1, gene2;

		return fitness;
	}

	public void checaMutacao() {
		int dado;
		int[] cadeia = this.genoma;

		for (int i = 0; i < cadeia.length; i++) {
			if (isMutavel()) {
//				 System.out.println("muta��o");
				cadeia[i] = mutacao(cadeia);

			}
		}

		this.genoma = cadeia;

	}

	private int mutacao(int[] cadeia) {
		
		int novoValor = prng.nextInt(Config.NUM_ALFABETO) + 1;
		for (int i = 0; i < cadeia.length; i++) {
			if(novoValor == cadeia[i]){
				novoValor = prng.nextInt(Config.NUM_ALFABETO) + 1;
				i = 0;
//				System.out.println("repetido");
			}
		}
		return novoValor;
	}

	private boolean isMutavel() {
		// Numero sorteado entre 0 e 100,
		// se ele for menor que a probabilidade significa que
		// que o sorteio caiu dentro da probabilidade
		int dado = prng.nextInt(100);
		// System.out.println(dado);
		// System.out.println((Config.PM * 100));
		return dado <= (Config.PM * 100);
	}
	
	


	public Individuo(Individuo other) {
		super();
		this.fitness = other.getFitness();
		this.genoma = other.getGenoma().clone();
		this.geracao = other.getGeracao();
		this.fDistancia = other.fDistancia;
		this.fPreco = other.fPreco;
		this.fCompatibilidade = other.fCompatibilidade;
		this.fAvaliacao = other.fAvaliacao;
	}

	@Override
	public String toString() {
		return "" + Arrays.toString(genoma) + " , fitness = "
				+ fitness ;
//				+ ", D = "+fDistancia+"Km, P = R$"+fPreco+" Com = "+fCompatibilidade+" A = "+fAvaliacao+" \n";
		// + Arrays.toString(cadeiaDecodificada)+ "\n";
	}
	
	public void decodificaAtrativo(){
		Atrativo[] atrativos = new Atrativo[genoma.length];
		for (int i = 0; i < genoma.length; i++) {
			atrativos[i] = Valores.ATRATIVOS.get(genoma[i]-1);
		}
		
		this.atrativos = atrativos;
	}

	public Atrativo[] getAtrativos() {
		return atrativos;
	}

	public void setAtrativos(Atrativo[] atrativos) {
		this.atrativos = atrativos;
	}

	public Double getDistanciaTotal() {
		return fDistancia;
	}

	public void setDistanciaTotal(Double distanciaTotal) {
		this.fDistancia = distanciaTotal;
	}

	public Double getPrecoTotal() {
		return fPreco;
	}

	public void setPrecoTotal(Double precoTotal) {
		this.fPreco = precoTotal;
	}

	public Double getAvaliacaoTotal() {
		return fAvaliacao;
	}

	public void setAvaliacaoTotal(Double avaliacaoTotal) {
		this.fAvaliacao = avaliacaoTotal;
	}

	public Double getCompatibilidadeTotal() {
		return fCompatibilidade;
	}

	public void setCompatibilidadeTotal(Double compatibilidadeTotal) {
		this.fCompatibilidade = compatibilidadeTotal;
	}

	public Double getfDistancia() {
		return fDistancia;
	}

	public void setfDistancia(Double fDistancia) {
		this.fDistancia = fDistancia;
	}

	public Double getfPreco() {
		return fPreco;
	}

	public void setfPreco(Double fPreco) {
		this.fPreco = fPreco;
	}

	public Double getfAvaliacao() {
		return fAvaliacao;
	}

	public void setfAvaliacao(Double fAvaliacao) {
		this.fAvaliacao = fAvaliacao;
	}

	public Double getfCompatibilidade() {
		return fCompatibilidade;
	}

	public void setfCompatibilidade(Double fCompatibilidade) {
		this.fCompatibilidade = fCompatibilidade;
	}
	
	
}
