package testes;

import java.util.ArrayList;
import java.util.List;

import AG.*;
import Atrativos.Valores;

public class Execucao {

	Solucao solucao = new Solucao(null);
	int tamPopulacao = Config.TAM_POP;
	int numGenes = Config.NUM_GENES;
	Populacao populacao = new Populacao();
	Populacao populacaoInicial, populacaoFinal;
	int melhorFitness = 0;
	int numGeracoes = 0;
	Individuo melhorIndividuo;
	int[] geracaoFitnes ;
	List<int[]> graf = new ArrayList<int[]>();

	public void executar() {
		
		

		
		System.out.println("e");

		populacao.Incializar(tamPopulacao, numGenes);

		// System.out.println(populacao);
		populacao.Avaliar(null);
		this.populacaoInicial = new Populacao(populacao);
		
		geracaoFitnes = new int[3+4];
		geracaoFitnes[0] = 0;
		geracaoFitnes[1] = populacao.getMelhorFitness();
		geracaoFitnes[2] = populacao.getPiorFitness();
		geracaoFitnes[3] = populacao.getMelhorIndividuo().getDistanciaTotal().intValue();
		geracaoFitnes[4] = populacao.getMelhorIndividuo().getPrecoTotal().intValue();
		geracaoFitnes[5] = populacao.getMelhorIndividuo().getCompatibilidadeTotal().intValue();
		geracaoFitnes[6] = populacao.getMelhorIndividuo().getAvaliacaoTotal().intValue();
	
		graf.add(geracaoFitnes);
		
		
			
		int fitnessAnterior = 0;
		int seguidos = 1;

		for (int i = 1; (seguidos < Config.K && i< Config.MAX_GERACOES) ; i++) {
			numGeracoes = i;
//			System.out.println("--------Gera��o #"+i+"--------");
			 //System.out.println("Popula��o na itera��o #"+i+" :\n"+populacao);
			//populacao.Avaliar(solucao);
			populacao.setGeracao(populacao.getGeracao() + 1);
			populacao = new Populacao(populacao.novaGeracao(numGenes));
//			System.out.println("\nPopula��o final na itera��o #" + i + " :\n"
//					+ populacao);
//			System.out.println("#"+i+": Melhor fitness nessa gera��o: "
//					+ populacao.getMelhorFitness() + " || Melhor fitness at� agora: "+melhorFitness);
			

//			System.out.println(i+","+populacao.getMelhorFitness());
			geracaoFitnes = new int[3+4];
			geracaoFitnes[0] = i;
			geracaoFitnes[1] = populacao.getMelhorFitness();
			geracaoFitnes[2] = populacao.getPiorFitness();
			geracaoFitnes[3] = populacao.getMelhorIndividuo().getDistanciaTotal().intValue();
			geracaoFitnes[4] = populacao.getMelhorIndividuo().getPrecoTotal().intValue();
			geracaoFitnes[5] = populacao.getMelhorIndividuo().getCompatibilidadeTotal().intValue();
			geracaoFitnes[6] = populacao.getMelhorIndividuo().getAvaliacaoTotal().intValue();

			graf.add(geracaoFitnes);
			
			if (populacao.getMelhorFitness() > melhorFitness) {
				this.melhorFitness = populacao.getMelhorFitness();
			}
			
			if (populacao.getMelhorFitness() == fitnessAnterior) {
				seguidos++;
			}else{
				seguidos = 1;
				fitnessAnterior = populacao.getMelhorFitness();
			}
		}
		
		this.populacaoFinal = new Populacao(populacao);
		this.melhorIndividuo = this.populacaoFinal.getMelhorIndividuo();
		
//		System.out.println("Popula��o Final:\n" + populacao);
		
		
		
	}
	
	
	
	





	@Override
	public String toString() {
		return "Ex: G:"
				+ numGeracoes + ", Melhor Individuo: " + melhorIndividuo + " \n";
	}





	public Individuo getMelhorIndividuo() {
		return melhorIndividuo;
	}



	public void setMelhorIndividuo(Individuo melhorIndividuo) {
		this.melhorIndividuo = melhorIndividuo;
	}



	public Solucao getSolucao() {
		return solucao;
	}

	public void setSolucao(Solucao solucao) {
		this.solucao = solucao;
	}

	public int getTamPopulacao() {
		return tamPopulacao;
	}

	public void setTamPopulacao(int tamPopulacao) {
		this.tamPopulacao = tamPopulacao;
	}

	public int getNumGenes() {
		return numGenes;
	}

	public void setNumGenes(int numGenes) {
		this.numGenes = numGenes;
	}

	public Populacao getPopulacao() {
		return populacao;
	}

	public void setPopulacao(Populacao populacao) {
		this.populacao = populacao;
	}

	public Populacao getPopulacaoInicial() {
		return populacaoInicial;
	}

	public void setPopulacaoInicial(Populacao populacaoInicial) {
		this.populacaoInicial = populacaoInicial;
	}

	public Populacao getPopulacaoFinal() {
		return populacaoFinal;
	}

	public void setPopulacaoFinal(Populacao populacaoFinal) {
		this.populacaoFinal = populacaoFinal;
	}

	public int getMelhorFitness() {
		return melhorFitness;
	}

	public void setMelhorFitness(int melhorFitness) {
		this.melhorFitness = melhorFitness;
	}

	public int getNumGeracoes() {
		return numGeracoes;
	}

	public void setNumGeracoes(int numGeracoes) {
		this.numGeracoes = numGeracoes;
	}









	public int[] getGeracaoFitnes() {
		return geracaoFitnes;
	}









	public void setGeracaoFitnes(int[] geracaoFitnes) {
		this.geracaoFitnes = geracaoFitnes;
	}









	public List<int[]> getGraf() {
		return graf;
	}









	public void setGraf(List<int[]> graf) {
		this.graf = graf;
	}





}
